package main.bean;

import kcp.Ukcp;
import main.protobuf.InputPackageOuterClass;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by JinMiao
 * 2019-07-08.
 */
public class Player {

    private static final AtomicInteger idGen = new AtomicInteger();

    private Ukcp ukcp;

    private int id ;

    private List<InputPackageOuterClass.InputPackage> inputPackages = new ArrayList<>();




    public Player(Ukcp ukcp) {
        this.ukcp = ukcp;
        this.id = idGen.incrementAndGet();
    }





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static AtomicInteger getIdGen() {
        return idGen;
    }

    public List<InputPackageOuterClass.InputPackage> getInputPackages() {
        return inputPackages;
    }

    public void setInputPackages(List<InputPackageOuterClass.InputPackage> inputPackages) {
        this.inputPackages = inputPackages;
    }

    public Ukcp getUkcp() {
        return ukcp;
    }

    public void setUkcp(Ukcp ukcp) {
        this.ukcp = ukcp;
    }
}
