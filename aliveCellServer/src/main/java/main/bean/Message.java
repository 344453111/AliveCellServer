package main.bean;

import com.google.protobuf.MessageLite;

/**
 * Created by JinMiao
 * 2019-07-24.
 */
public class Message {

    private int actionId;

    private MessageLite body;

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public <T> T getBody() {
        return (T) body;
    }



    public void setBody(MessageLite body) {
        this.body = body;
    }

    public void clear(){
        actionId = 0;
        body = null;
    }

    @Override
    public String toString() {
        return "Message{" +
                "actionId=" + actionId +
                ", body=" + body +
                '}';
    }
}
