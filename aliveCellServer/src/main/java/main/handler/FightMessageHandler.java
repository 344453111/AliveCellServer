package main.handler;

import com.google.protobuf.GeneratedMessageV3;
import main.bean.Message;
import main.bean.Player;
import main.protobuf.InputPackageOuterClass;
import main.service.GameCache;
import main.service.IHandler;
import main.service.MessageId;

/**
 * Created by JinMiao
 * 2019-08-02.
 */
public class FightMessageHandler implements IHandler {
    @Override
    public void handler(Player player, Message msg, GameCache gameCache) throws Throwable {
        InputPackageOuterClass.InputPackage inputPackage = msg.getBody();
        player.getInputPackages().add(inputPackage);
    }

    @Override
    public short messageId() {
        return MessageId.FightMessage_C2S;
    }

    @Override
    public Class<? extends GeneratedMessageV3> initBodyClass() {
        return InputPackageOuterClass.InputPackage.class;
    }

    @Override
    public String getExecuteName() {
        return null;
    }
}
