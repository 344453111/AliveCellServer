package main.handler;

import com.google.protobuf.GeneratedMessageV3;
import main.bean.Message;
import main.bean.Player;
import main.service.GameCache;
import main.service.IHandler;
import main.service.MessageId;
import main.utils.GameUtils;

/**
 * Created by JinMiao
 * 2019-08-02.
 */
public class HeartBeatHandler implements IHandler {

    @Override
    public void handler(Player player, Message msg,GameCache gameCache) {
        msg.setActionId(MessageId.HeartBeat_S2C);
        GameUtils.sendTo(msg,player);
    }


    @Override
    public short messageId() {
        return MessageId.HeartBeat_C2S;
    }

    @Override
    public  Class<? extends GeneratedMessageV3> initBodyClass() {
        return null;
    }

    @Override
    public String getExecuteName() {
        return null;
    }
}
