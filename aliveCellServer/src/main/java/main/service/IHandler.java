package main.service;


import com.google.protobuf.GeneratedMessageV3;
import main.bean.Message;
import main.bean.Player;

public interface IHandler{
	void handler(Player player,Message msg,GameCache gameCache) throws Throwable;

	/**
	 * 获取消息id
	 * @return
	 */
	short messageId();
	
	/**
	 * 初始化消息体对象
	 * @return
	 */
	Class<? extends GeneratedMessageV3> initBodyClass();
	
	/**获得线程名字**/
	String getExecuteName();
	
}
