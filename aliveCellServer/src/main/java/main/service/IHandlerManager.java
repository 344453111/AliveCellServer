package main.service;

/**
 * 业务处理类管理类  
 * @author King
 *
 */
public interface IHandlerManager
{

	/**
	 * 增加一个业务处理类
	 * @param actionId
	 * @param handler
	 */
	void addHandler(int actionId, IHandler handler);

	/**
	 * 获取一个业务处理类
	 * @param actionId
	 * @return
	 */
	IHandler getHandler(int actionId);

}