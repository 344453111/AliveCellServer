package main.service;

import main.bean.Room;
import threadPool.thread.DisruptorExecutorPool;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by JinMiao
 * 2019-08-02.
 */
public class GameCache {

    public Map<Integer, Room> playerRooms = new ConcurrentHashMap<>();

    public DisruptorExecutorPool disruptorExecutorPool = new DisruptorExecutorPool();


}
