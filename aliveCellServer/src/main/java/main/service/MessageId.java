package main.service;

/**
 * Created by JinMiao
 * 2019-07-24.
 */
public class MessageId {

    public static final int HeartBeat_C2S = -1;
    public static final int HeartBeat_S2C = -2;

    public static final int LOST_CONNECT= -3;

    public static final int JoinRoom_C2S =1;
    public static final int JoinRoom_S2C =2;

    public static final int FightMessage_C2S=3;
    public static final int FightMessage_S2C=4;
}
