package main.service;

import java.util.HashMap;
import java.util.Map;


/**
 * 业务处理管理类
 * @author jinmiao
 *
 */
public class HandlerManager implements IHandlerManager
{

	private final Map<Integer, IHandler> handlerMap = new HashMap<>();

	
	/**
	 * 增加一个业务处理类
	 * @param handler
	 */
	public void addHandler(int actionId,IHandler handler)
	{
		handlerMap.put(actionId, handler);
	}
	
	/**
	 * 获取一个业务处理类
	 * @return
	 */
	public IHandler getHandler(int actionId)
	{
		final IHandler handlers = handlerMap.get(actionId);
		return handlers;
	}
}
